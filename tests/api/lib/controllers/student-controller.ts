import { ApiRequest } from "../request";
let baseUrl: string = global.appConfig.baseUrl;



export class StudentLoginController {
    async login(email, password) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`auth/login`) 
            .body( {"email": email, "password": password} )   
            .send()
        return response;
    }
}


export class AboutStudentGet {
    async Myinfo(token) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`student/info/`) 
            .bearerToken(token)
            .send()
        return response;
    }
}


export class AboutStudentSet {
    async setstudentinfo(token, id) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`student/`) 
            .bearerToken(token)
            .body(
                {
                    "id": id,
                    "firstName": "Puss",
                    "lastName": "Inboots",
                    "avatar": null,
                    "location": "Åland Islands",
                    "company": "eye",
                    "job": "eye",
                    "website": "",
                    "biography": "",
                    "direction": "Developer",
                    "experience": 1,
                    "level": "Intermediate",
                    "industry": "Software Products",
                    "role": "IT Support",
                    "employment": "Self-employed",
                    "education": "High School",
                    "year": 1951,
                    "uploadImage": null,
                    "tags": []
                }
            )
            .send()
        return response;
    }
}


export class StudentUserMe {
    async studentme(token) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`user/me`) 
            .bearerToken(token)
            .send()
        return response;
    }
}


export class GetAllCourses {
    async getAllCourses() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`course/all`) 
            .send();
        return response;
    }
}

export class GetCourseWithId {
    async getbyid(id) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url("course/"+id+"/info") 
            .send();
        return response;
    }
}



export class FollowCourse {
    async followCourse(token, courseid) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url("favorite/change?id="+courseid+"&type=COURSE") 
            .bearerToken(token)
            .body(
                {  
                    "id" : courseid,
                    "type": "COURSE"  
                }
            )
            .send()
        return response;
    }
}