import { ApiRequest } from "../request";

let baseUrl: string = global.appConfig.baseUrl;

export class LoginControlller {
    async login(email, password) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`auth/login`)
            .body({ "email": email, "password": password })
            .send()
        return response;
    }
}


export class AboutAuthorGet {
    async Myinfo(token) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`author/`)
            .bearerToken(token)
            .send()
        return response;
    }
}


export class AboutAuthorSet {
    async setinfo(token, id, userId) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`author`)
            .bearerToken(token)
            .body(
                {
                    "avatar": "string",
                    "biography": "string",
                    "company": "string",
                    "firstName": "Shrek",
                    "id": id,
                    "job": "string",
                    "lastName": "Theorge",
                    "location": "Swamp",
                    "twitter": "string",
                    "userId": userId,
                    "website": "string"
                }
            )
            .send()
        return response;
    }

}


export class UserMe {
    async userme(token) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`user/me`)
            .bearerToken(token)
            .send()
        return response;
    }
}


export class Overview {
    async overview(token, id) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`author/overview/` + id)
            .bearerToken(token)
            .send()
        return response;
    }
}



export class PostArticle {
    async postarticle(token) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`article/`)
            .bearerToken(token)
            .body(
                {
                    "name": "swamp",
                    "image": "https://knewless.tk/assets/images/30b78a06-7a6f-4b51-b939-fc20dc821410.jpg",
                    "text": "<p>45454</p>\n",
                    "uploadImage": {}
                }
            )
            .send()
        return response;
    }
}



export class GetArticle {
    async getarticle(token) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url("article/author")
            .bearerToken(token)
            .send()
        return response;
    }
}



export class GetArticleById {
    async getarticlebyid(token, articleId) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url("article/" + articleId)
            .bearerToken(token)
            .send()
        return response;
    }
}


export class PostArticleComment {
    async postarticlecomment(token, articleId) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url("article_comment")
            .bearerToken(token)
            .body(
                {
                    "text": "comment 12121",
                    "articleId": articleId
                }
            )
            .send()
        return response;
    }

}




export class GetArticleComment {
    async getarticlecomment(token, articleId) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url("article_comment/of/" + articleId + "?size=200")
            .bearerToken(token)
            .send()
        return response;
    }
}