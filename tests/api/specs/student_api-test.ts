import { expect } from "chai";
import { checkStatusCode, checkResponseBodyStatus, checkResponseBodyMessage, checkResponseTime, checkResponseBodyError } from "../../helpers/functionsForCheking.helper";
import { StudentLoginController, AboutStudentGet, AboutStudentSet, StudentUserMe, GetAllCourses, GetCourseWithId, FollowCourse } from "../lib/controllers/student-controller";


var chai = require('chai');
chai.use(require('chai-json-schema'));
const schemas = require('./data/student_schema');


const studentlogin = new StudentLoginController();
const aboutstudent = new AboutStudentGet();
const studentinfoset = new AboutStudentSet();
const studentme = new StudentUserMe();
const allcourses = new GetAllCourses();
const getcoursebyid = new GetCourseWithId();
const followcourse = new FollowCourse();


describe("Student sequence refactored", () => {;
    let token;
    let id;
    let courseid;
    let email: string = 'qtilxlcvtrgtzqnthu@nthrw.com';
    let password: string = 'Shrek123';


    it(`Check environment`, async () => {
        console.log(`Now we are using the ${global.appConfig.envName}`);
    });


    describe('Using test data for login', () => {
        let invalidCredentialsDataSet = [
            { email: 'qtilxlcvtrgtzqnthu@nthrw.com', password: '      ' },
            { email: 'qtilxlcvtrgtzqnthu@nthrw.com', password: 'Shrek123 ' },
            { email: 'qtilxlcvtrgtzqnthu@nthrw.com', password: 'Shre 123k' },
            { email: 'qtilxlcvtrgtzqnthu@nthrw.com', password: 'kekwlmao' },
            { email: 'qtilxlcvtrgtzqnthu@nthrw.com', password: 'qtilxlcvtrgtzqnthu@nthrw.com' },
            { email: 'qtilxlcvtrgtzqnthu@nthrw.com ', password: 'Shrek123' },
            { email: 'qtilxlcvtrgtzqnthu@nthrw.com  ', password: 'kappa' },
        ];
    
        invalidCredentialsDataSet.forEach((credentials) => {
            it(`login using invalid credentials : ${credentials.email} + ${credentials.password}`, async () => {
                let response = await studentlogin.login(credentials.email, credentials.password);
    
                checkStatusCode(response, 401);
                checkResponseBodyStatus(response, 'UNAUTHORIZED');
                checkResponseBodyMessage(response, 'Bad credentials');
                checkResponseTime(response, 3000);
            });
        });
    });
    

    before('Getting access token to log in', async () => {
        let response = await studentlogin.login(email, password);

        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_login);

        //expect(response.statusCode, `Status Code should be 200`).to.be.equal(400);  UNCOMMENT ME TO GET NEGATIVE TEST #1
        //expect(response.timings.phases.total, 'Response time should be less than 5s').to.be.lessThan(5000);  UNCOMMENT ME TO GET NEGATIVE TEST #2
        //expect(response.body).to.be.jsonSchema(schemas.schema_myinfo);  UNCOMMENT ME TO GET NEGATIVE TEST #3

        token = response.body.accessToken;
    });


    it('Student info',async () => {
        let response = await aboutstudent.Myinfo(token);

        //console.log(response.body);
 
        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_studinfo);

        id = response.body.id;
    });


    it('Set student info',async () => {
        let response = await studentinfoset.setstudentinfo(token, id);

        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_setinfo);
    });


    describe('Trying to get STUDENT info with the wrong token', () => {
        let invalidTokens = [
            { token: 'invalidtoken' },
            { token: 'kekw' },
            { token: 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJiM2YyZGNkZi1hNzA5LTQyNzktYjc1MC1hNmFiMWEyMDRiOGIiLCJpYXQiOjE2NTg5OTUwMjQsImV4cCI6MTY1OTA4MTQyNH0.i7y_U1RKwFOLyvlzCdnfpQhJNH9f94AKM2-mpMiuGU6d6s7dKmYKwiXzTe76Tt2p_LM2PTIET-yVEIz8Gxyzw' },
            { token: '               ' },
            { token: " i ain't a token " },
        ];
    
        invalidTokens.forEach((tokens) => {
            it(`Getting info with wrong token : ${tokens.token}`, async () => {
                let response = await studentme.studentme(tokens);
    
                checkResponseBodyError(response, 'Unauthorized');
                checkResponseBodyStatus(response, 401);
                checkResponseBodyMessage(response, '');
                checkResponseTime(response, 3000);
            });
        });
    });


    it('Student userme',async () => {
        let response = await studentme.studentme(token);

        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_studuserme);
    });


    it('View all courses',async () => {
        let response = await allcourses.getAllCourses();

        //console.log(response.body[0]);

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body[0]).to.be.jsonSchema(schemas.schema_viewcourses);

        courseid = response.body[0].id;
    });


    describe('Trying to get info about the course with the right ID after QA students have broken the server', () => {
        let validCourseId = [
            { id: '67b9cef5-0c8c-4728-9246-a3973c18f6f1' },
            { id: '0a0ae8aa-8166-4138-a938-438c65f40f42'},
            { id: '1db5668e-500e-4237-a0bb-79db980054ac'}
        ];
    
        validCourseId.forEach((courseidinarray) => {
            it(`Getting info with right ID : ${courseidinarray.id}`, async () => {
                let response = await getcoursebyid.getbyid(courseidinarray.id);

                checkStatusCode(response, 500);
                checkResponseBodyStatus(response, 'INTERNAL_SERVER_ERROR');
                checkResponseBodyMessage(response, 'Unexpected error');
                checkResponseTime(response, 5000);
            });
        });
    });



    it('Get info about the course by id',async () => {
        let response = await getcoursebyid.getbyid(courseid);

        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
        expect(response.body).to.be.jsonSchema(schemas.schema_getcourseinfobyid);
    });



    it('Follow the course',async () => {
        let response = await followcourse.followCourse(token, courseid);

        //console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 5000);
    });

    afterEach(function () {
        console.log('Testing after each hook - student seq');
    });

});